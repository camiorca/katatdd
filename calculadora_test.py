from unittest import TestCase

from calculadora import  Calculadora

__author__ = 'ca.ortiz1633'

class CalculadoraTest(TestCase):
    def test_sumar(self):
        self.assertEqual(Calculadora().sumar(""), 0, "Empty string")

    def test_sumar_unacadena(self):
        self.assertEqual(Calculadora().sumar("1"), 1, "A number")

    def test_sumar_numerodiferente(self):
        self.assertEqual(Calculadora().sumar("1"), 1, "A number")
        self.assertEqual(Calculadora().sumar("2"), 2, "A number")

    def test_sumar_addtwonumbers(self):
        self.assertEqual(Calculadora().sumar("1,2"), 3, "A sum of two numbers")

    def test_sumar_addseveralnumbers(self):
        self.assertEqual(Calculadora().sumar("1,2,3,4"), 10, "A sum of several numbers")

    def test_sumar_addseveralnumbersseveralseparators(self):
        self.assertEqual(Calculadora().sumar("5,2&4:1:2&8"), 22, "A sum of several numbers with multiple separators")
