__author__ = 'ca.ortiz1633'

class Calculadora:
    def sumar(self, cadena):
        if cadena == "":
            return 0
        elif "," in cadena or "&" in cadena or ":" in cadena:
            numbers = cadena.replace("&", ",").replace(":", ",").split(',')
            res = 0
            for number in numbers:
                res = res + int(number)
            return res
        else:
            return int(cadena)
